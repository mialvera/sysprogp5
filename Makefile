# Este es el archivo Makefile de la práctica, editarlo como sea neceario
CFLAGS=-lm -I./include
CC=gcc
DEPS=./include/vector.h
DEP1=./lib/vector.c
MAIN=./src/main.c
OBJ = vector.o
OBJ1= main.o 

# Target que se ejecutará por defecto con el comando make
all: dynamic

vector.o: $(DEP1) $(DEPS)
	$(CC) -c $(DEP1) $(CFLAGS)
main.o: $(MAIN) $(DEPS)
	$(CC) -c $(MAIN) $(CFLAGS)

libvector.a: $(OBJ)
	ar rcs libvector.a $(OBJ)

# Target que compilará el proyecto usando librerías estáticas
static: $(OBJ) $(OBJ1) libvector.a
	$(CC) -static -o vectorEstatico main.o ./libvector.a -lm

# Target que compilará el proyecto usando librerías dinámicas
dynamic: $(DEP1) $(OBJ)
	$(CC) -shared -fPIC -o libvector.so $(DEP1) $(CFLAGS)
	$(CC) -o vectorDinamico $(MAIN) ./libvector.so $(CFLAGS)

# Limpiar archivos temporales
clean:
	rm *.o
	ls -p | grep -v "/\|\.\|Makefile" | xargs rm
	echo 'Limpieza exitosa'
