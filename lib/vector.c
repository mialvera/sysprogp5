/* implementar aquí las funciones requeridas */
#include <vector.h>
#include <math.h>

float dotproduct(vector3D v1, vector3D v2) {

	float resultado;

	resultado = v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;

	return resultado;

}

vector3D crossproduct(vector3D v1, vector3D v2) {

	float compX, compY, compZ;

	compX = v1.y * v2.z - v2.y * v1.z;

	compY = v1.x * v2.z - v2.x * v1.z;

	compZ = v1.x * v2.y - v2.x * v1.y;

	vector3D productoCruz;

	productoCruz.x = compX;

	productoCruz.y = - compY;

	productoCruz.z = compZ;

	return productoCruz;

}

float magnitud(vector3D v) {

	float expresion, magnitud;

	expresion = dotproduct(v, v);

	magnitud = sqrt(expresion);

	return magnitud;

}

int esOrtogonal(vector3D v1, vector3D v2) {

	float magV1, magV2;

	magV1 = magnitud(v1);
	magV2 = magnitud(v2);

	if (magV1 > 0 && magV2 > 0) {
		
		float productoPunto;

		productoPunto = dotproduct(v1, v2);

		if (productoPunto == 0) {
			return 1;
		}
	}

	return 0;

}
