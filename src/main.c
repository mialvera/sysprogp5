#include<stdio.h>
#include<vector.h>

#define VECTORES 2
vector3D ingresarDatos();

int main() {
	
    struct vector3D vectores[VECTORES];
	
	printf("Operaciones entre vectores (3D)\n");
	
	for (int i = 0; i < VECTORES; i++) {
		printf("Vector %d:\n", i + 1);		
		vector3D v;
		v = ingresarDatos();
		vectores[i] = v;
	}
	
	vector3D pvec;
	float pdot, mv1, mv2;
	int ort;

	pdot = dotproduct(vectores[0], vectores[1]);
	pvec = crossproduct(vectores[0], vectores[1]);
	mv1 = magnitud(vectores[0]);
	mv2 = magnitud(vectores[1]);
	
	ort = esOrtogonal(vectores[1], vectores[2]);

	printf("El producto escalar entre vector 1 y vector 2 es: %.2f\n", pdot);
	printf("La magnitud de vector 1 es %.2f y la magnitud del vector 2 es %.2f\n", mv1, mv2);
	printf("El producto vectorial entre vector 1 y vector 2 es: (%.2f, %.2f, %.2f)\n", pvec.x, pvec.y, pvec.z);

	if(ort == 1){
		printf("El vector 1 y 2 son ortogonales\n");
	}
	else {
		printf("El vector 1 y 2 no son ortogonales\n");
	}

}

vector3D ingresarDatos(){
	
	vector3D v;	
	
	printf("Ingresar coordenada x: ");
	scanf("%f", &v.x);
	
	printf("Ingresar coordenada y: ");
	scanf("%f", &v.y);

	printf("Ingresar coordenada z: ");
	scanf("%f", &v.z);

	printf("\n");

	return v;

}
